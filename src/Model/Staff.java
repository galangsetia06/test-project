package Model;
public class Staff {
    private String stf_id;
    private String stf_namadepan;
    private String stf_namabelakang;
    private String stf_alamat;
    private String stf_notlp;
    
   

    public Staff() {
    }

    public Staff(String stf_id, String stf_namadepan, String stf_namabelakang, String stf_alamat, String stf_notlp) {
        this.stf_id = stf_id;
        this.stf_namadepan = stf_namadepan;
        this.stf_namabelakang = stf_namabelakang;
        this.stf_alamat = stf_alamat;
        this.stf_notlp = stf_notlp;
      
        
    }

    public String getStf_id() {
        return stf_id;
    }

    public void setStf_id(String stf_id) {
        this.stf_id = stf_id;
    }

    public String getStf_namadepan() {
        return stf_namadepan;
    }

    public void setStf_namadepan(String stf_namadepan) {
        this.stf_namadepan = stf_namadepan;
    }

    public String getStf_namabelakang() {
        return stf_namabelakang;
    }

    public void setStf_namabelakang(String stf_namabelakang) {
        this.stf_namabelakang = stf_namabelakang;
    }

    public String getStf_alamat() {
        return stf_alamat;
    }

    public void setStf_alamat(String stf_alamat) {
        this.stf_alamat = stf_alamat;
    }

    public String getStf_notlp() {
        return stf_notlp;
    }

    public void setStf_notlp(String stf_notlp) {
        this.stf_notlp = stf_notlp;
    }
}

    
  