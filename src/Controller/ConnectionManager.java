
package Controller;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Staff
 */
public class ConnectionManager {
    private Connection con;
    private String Driver = "com.mysql.jdbc.Driver";
    private String url = "jdbc:mysql://localhost:3306/latihan1";
    private String Username = "root";
    private String Password = "";
    
    public Connection LogOn(){
        try {
            Class.forName(Driver).newInstance();
            con = DriverManager.getConnection(url, Username, Password);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return con;
    }
    
    public void LogOff(){
        try {
            con.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
}
