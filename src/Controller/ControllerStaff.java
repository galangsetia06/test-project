package Controller;
import Model.Staff;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ControllerStaff {
    Scanner in = new Scanner(System.in);
    ConnectionManager conMan = new ConnectionManager();
    Connection con = conMan.LogOn();
    
    
    public void insertStaff(String stfId, String stfNamaDepan,
            String stfNamaBelakang, String stfAlamat, String stfNoTlp) {
       String query = "INSERT INTO staff (stf_id, stf_namadepan ,stf_namabelakang, stf_alamat, stf_notlp) values " 
                + "('"+stfId+"','"+stfNamaDepan+"','"+stfNamaBelakang+"'," 
                + "'"+stfAlamat+"','"+stfNoTlp+"')"; 
        try {
            Statement stm = con.createStatement();
            int result = stm.executeUpdate(query);
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
                
    }
    
    public List<Staff> tampil() {
        List<Staff> listStf = new ArrayList();
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM staff");
            while(rs.next()) {
                Staff Stf = new Staff();
               
                Stf.setStf_id(rs.getString("stf_id"));
                Stf.setStf_namadepan(rs.getString("stf_namadepan"));
                Stf.setStf_namabelakang(rs.getString("stf_namabelakang"));
                Stf.setStf_alamat(rs.getString("stf_alamat"));
                Stf.setStf_notlp(rs.getString("stf_notlp"));
                
               
               
                listStf.add(Stf);
            }
        } catch (SQLException ex){
            System.out.println(ex.toString());
        }
        return listStf;
    }
    
    public void UpdateStaff(String stfId, String stfNamaDepan,
            String stfNamaBelakang, String stfAlamat, String stfNoTlp) { {
        int result = 0; 
      String query = "UPDATE staff set stf_namadepan= '" + stfNamaDepan + "', "
                + "stf_namabelakang='" + stfNamaBelakang + "',stf_alamat ='" + stfAlamat + "', "
                + "stf_notlp= '" + stfNoTlp + "', "
                + "WHERE stf_id = '" + stfId + "'";
      try {
          System.out.println(query);
            Statement stm = con.createStatement();
           result = stm.executeUpdate(query);
        } catch (SQLException ex) {
            System.out.println("Update Gagal");
        }
    }
    
    
    public void deleteStaff(String stf_Id){
        int result = 0;
         String query = "Delete From staff WHERE stf_id =  "+stf_Id+""; 
          try {
            Statement stm = con.createStatement();
            result = stm.executeUpdate(query);
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
   }
}
